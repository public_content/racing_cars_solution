package com.jnanacetana.crio.racingcars.adapter.csvprocessor;

import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PartCSVProcessor implements CSVProcessor{

    private RacingRepository racingRepository;

    public PartCSVProcessor(RacingRepository racingRepository) {
        this.racingRepository = racingRepository;
    }

    @Override
    public void execute(FileReader fileReader) throws IOException {
        String line = "";
        String splitBy = ",";
        try {
            int counter = 0;
            BufferedReader br = new BufferedReader(fileReader);
            while((line = br.readLine()) != null){
                if (counter == 0){
                    counter =1;
                    continue;
                }
                String[] tokens = line.split(splitBy);
                String part_list_id = tokens[0];
                String part_id = tokens[1];
                Integer price = Integer.parseInt(tokens[2]);
                Integer speedBoost = Integer.parseInt(tokens[3]);
            racingRepository.addPart(part_list_id,part_id,price,speedBoost);
            }
            br.close();
        } catch(IOException e){
            throw new IOException(e);
        }
    }
}
