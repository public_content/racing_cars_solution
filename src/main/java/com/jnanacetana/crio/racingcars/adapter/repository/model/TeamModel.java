package com.jnanacetana.crio.racingcars.adapter.repository.model;

import java.util.List;

public class TeamModel {
    private String teamId;
    private String teamName;
    private List<String> carsIdList;
    private Integer funds;

    public TeamModel(String teamId, String teamName, List<String> carsIdList, Integer funds) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.carsIdList = carsIdList;
        this.funds = funds;
    }

    public List<String> getCarsIdList() {
        return carsIdList;
    }

    public void setCarsIdList(List<String> carsIdList) {
        this.carsIdList = carsIdList;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }


    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    @Override
    public String toString() {
        return "TeamModel{" +
                "teamId='" + teamId + '\'' +
                ", teamName='" + teamName + '\'' +
                ", carsIdList=" + carsIdList +
                ", funds=" + funds +
                '}';
    }
}
