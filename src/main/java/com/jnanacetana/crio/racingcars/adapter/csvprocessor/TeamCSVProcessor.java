package com.jnanacetana.crio.racingcars.adapter.csvprocessor;

import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TeamCSVProcessor implements CSVProcessor{

    private RacingRepository racingRepository;

    public TeamCSVProcessor(RacingRepository racingRepository) {
        this.racingRepository = racingRepository;
    }

    @Override
    public void execute(FileReader fileReader) throws IOException {
        String line = "";
        String splitBy = ",";
        try {
            int counter = 0;
            BufferedReader br = new BufferedReader(fileReader);
            while((line = br.readLine()) != null){
                if (counter == 0){
                    counter =1;
                    continue;
                }
                String[] tokens = line.split(splitBy);
                String team_id = tokens[0];
                String team_name = tokens[1];
                String cars = tokens[2];
                Integer funds = Integer.parseInt(tokens[3]);
                racingRepository.addTeam(team_id,team_name,cars,funds);
            }
            br.close();
        } catch(IOException e){
            throw new IOException(e);
        }
    }
}
