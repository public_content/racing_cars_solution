package com.jnanacetana.crio.racingcars.adapter.csvprocessor;

import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CarCSVProcessor implements CSVProcessor{

    private RacingRepository racingRepository;

    public CarCSVProcessor(RacingRepository racingRepository) {
        this.racingRepository = racingRepository;
    }

    @Override
    public void execute(FileReader fileReader) throws IOException {
        String line = "";
        String splitBy = ",";
        try {
            int counter = 0;
            BufferedReader br = new BufferedReader(fileReader);
            while((line = br.readLine()) != null){
                if (counter == 0){
                    counter =1;
                    continue;
                }
                String[] tokens = line.split(splitBy);
                String car_id = tokens[0];
                String car_name = tokens[1];
                Integer base_speed = Integer.parseInt(tokens[2]);
                Integer top_speed = Integer.parseInt(tokens[3]);
                String part_list_id = tokens[4];
                racingRepository.addCar(car_id,car_name,base_speed,top_speed,part_list_id);
            }
            br.close();
        } catch(IOException e){
            throw new IOException(e);
        }
    }
}
