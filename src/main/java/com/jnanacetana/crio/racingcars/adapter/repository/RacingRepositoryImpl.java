package com.jnanacetana.crio.racingcars.adapter.repository;

import com.jnanacetana.crio.racingcars.adapter.repository.model.CarModel;
import com.jnanacetana.crio.racingcars.adapter.repository.model.PartModel;
import com.jnanacetana.crio.racingcars.adapter.repository.model.TeamModel;
import com.jnanacetana.crio.racingcars.domain.entity.Car;
import com.jnanacetana.crio.racingcars.domain.entity.Part;
import com.jnanacetana.crio.racingcars.domain.entity.Team;
import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

import java.util.*;
import java.util.stream.Collectors;

public class RacingRepositoryImpl implements RacingRepository {
    Map<String, TeamModel> teamMap = new HashMap<>();
    Map<String, CarModel> carMap = new HashMap<>();
    Map<String, List<PartModel>> partMap = new HashMap<>();

    @Override
    public List<Team> findAllTeams() {
        List<Team> teamList = new ArrayList<>();
        teamMap.forEach((k,t) -> {
            Team team = new Team();
            team.setId(t.getTeamId());
            team.setName(t.getTeamName());
            team.setFunds(t.getFunds());
            List<Car> carList = new ArrayList<>();
            t.getCarsIdList().forEach((carId)->{
                CarModel carModel = carMap.get(carId);
                Car car = new Car();
                car.setId(carModel.getCarId());
                car.setName(carModel.getCarName());
                car.setBaseSpeed(carModel.getBaseSpeed());
                car.setTopSpeed(carModel.getTopSpeed());
                List<PartModel> partModelList = partMap.get(carModel.getPartListId());
                List<Part> partList = partModelList
                        .stream()
                        .map(part -> new Part(part.getPartId(),
                                        part.getPrice(),
                                        part.getSpeedBoost()))
                        .collect(Collectors.toList());
                car.setParts(partList);
                carList.add(car);
            });
            team.setCars(carList);
            teamList.add(team);
        });
        return teamList;
    }

    @Override
    public void addTeam(String teamId, String teamName, String carId, Integer funds){
        if(teamMap.containsKey(teamId)){
            TeamModel t = teamMap.get(teamId);
            List<String> carsIdList = t.getCarsIdList();
            carsIdList.add(carId);
            t.setCarsIdList(carsIdList);
            teamMap.put(teamId,t);
            return;
        }
        List <String> carIds = new ArrayList<>();
        carIds.add(carId);
        teamMap.put(teamId,new TeamModel(teamId,teamName,carIds,funds));
    }

    @Override
    public void addCar(String carId, String carName, Integer baseSpeed, Integer topSpeed, String partListId){
        if(carMap.containsKey(carId)){
            return;
        }
        carMap.put(carId,new CarModel(carId,carName,baseSpeed,topSpeed,partListId));
    }

    @Override
    public void addPart(String partListId, String partId, Integer price, Integer speedBoost){
        if(partMap.containsKey(partListId)){
            List<PartModel> partModelList = partMap.get(partListId);
            partModelList.add(new PartModel(partListId,partId,price,speedBoost));
            partMap.put(partListId,partModelList);
            return;
        }
        List<PartModel> partModelList = new ArrayList<>();
        partModelList.add(new PartModel(partListId,partId,price,speedBoost));
        partMap.put(partListId,partModelList);
    }
}
