package com.jnanacetana.crio.racingcars.adapter.command;

import java.util.List;

public interface Command {
    public void execute(List<String> params);
}
