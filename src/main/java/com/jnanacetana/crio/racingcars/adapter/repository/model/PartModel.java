package com.jnanacetana.crio.racingcars.adapter.repository.model;

public class PartModel {
    private String partListId;
    private String partId;
    private Integer price;
    private Integer speedBoost;

    public PartModel(String partListId, String partId, Integer price, Integer speedBoost) {
        this.partListId = partListId;
        this.partId = partId;
        this.price = price;
        this.speedBoost = speedBoost;
    }

    public String getPartListId() {
        return partListId;
    }

    public void setPartListId(String partListId) {
        this.partListId = partListId;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSpeedBoost() {
        return speedBoost;
    }

    public void setSpeedBoost(Integer speedBoost) {
        this.speedBoost = speedBoost;
    }

    @Override
    public String toString() {
        return "PartModel{" +
                "partListId='" + partListId + '\'' +
                ", partId='" + partId + '\'' +
                ", price='" + price + '\'' +
                ", speedBoost='" + speedBoost + '\'' +
                '}';
    }
}
