package com.jnanacetana.crio.racingcars.adapter.repository.model;

public class CarModel {
    private String carId;
    private String carName;
    private Integer baseSpeed;
    private Integer topSpeed;
    private String partListId;

    public CarModel(String carId, String carName, Integer baseSpeed, Integer topSpeed, String partListId) {
        this.carId = carId;
        this.carName = carName;
        this.baseSpeed = baseSpeed;
        this.topSpeed = topSpeed;
        this.partListId = partListId;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Integer getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(Integer baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    public Integer getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(Integer topSpeed) {
        this.topSpeed = topSpeed;
    }

    public String getPartListId() {
        return partListId;
    }

    public void setPartListId(String partListId) {
        this.partListId = partListId;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "carId='" + carId + '\'' +
                ", carName='" + carName + '\'' +
                ", baseSpeed=" + baseSpeed +
                ", topSpeed=" + topSpeed +
                ", partListId='" + partListId + '\'' +
                '}';
    }
}

