package com.jnanacetana.crio.racingcars.adapter.command;

import com.jnanacetana.crio.racingcars.usecase.RaceWinnerUseCase;
import com.jnanacetana.crio.racingcars.usecase.port.RaceWinnerResponse;

import java.util.List;

public class GetWinnerCommand implements Command{
    private RaceWinnerUseCase raceWinnerUseCase;

    public GetWinnerCommand(RaceWinnerUseCase raceWinnerUseCase) {
        this.raceWinnerUseCase = raceWinnerUseCase;
    }

    @Override
    public void execute(List<String> params) {
        RaceWinnerResponse raceWinnerResponse = raceWinnerUseCase.getRaceWinner();
        System.out.println(raceWinnerResponse.toString());
    }
}
