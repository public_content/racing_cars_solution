package com.jnanacetana.crio.racingcars.adapter.csvprocessor;

import java.io.FileReader;
import java.io.IOException;

public interface CSVProcessor {
    void execute(FileReader fileReader) throws IOException;
}
