package com.jnanacetana.crio.racingcars.domain.entity;

import java.util.List;

public class Car {
    private String id;
    private String name;
    private Integer baseSpeed;
    private Integer topSpeed;
    private List<Part> parts;

    public Car(){}

    public Car(String id, String name, Integer baseSpeed, Integer topSpeed, List<Part> parts) {
        this.id = id;
        this.name = name;
        this.baseSpeed = baseSpeed;
        this.topSpeed = topSpeed;
        this.parts = parts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(Integer baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    public Integer getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(Integer topSpeed) {
        this.topSpeed = topSpeed;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", baseSpeed=" + baseSpeed +
                ", topSpeed=" + topSpeed +
                ", parts=" + parts +
                '}';
    }
}
