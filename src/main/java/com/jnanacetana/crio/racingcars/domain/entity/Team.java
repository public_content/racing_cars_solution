package com.jnanacetana.crio.racingcars.domain.entity;

import java.util.List;

public class Team {
    private String id;
    private String name;
    private Integer funds;
    private List<Car> cars;

    public Team(){}

    public Team(String id, String name, Integer funds, List<Car> cars) {
        this.id = id;
        this.name = name;
        this.funds = funds;
        this.cars = cars;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", funds=" + funds +
                ", cars=" + cars +
                '}';
    }
}
