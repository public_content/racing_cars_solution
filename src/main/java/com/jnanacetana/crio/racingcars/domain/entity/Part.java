package com.jnanacetana.crio.racingcars.domain.entity;

public class Part {
    private String partId;
    private Integer price;
    private Integer speedBoost;

    public Part(String partId, Integer price, Integer speedBoost) {
        this.partId = partId;
        this.price = price;
        this.speedBoost = speedBoost;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSpeedBoost() {
        return speedBoost;
    }

    public void setSpeedBoost(Integer speedBoost) {
        this.speedBoost = speedBoost;
    }

    @Override
    public String toString() {
        return "Part{" +
                "partId='" + partId + '\'' +
                ", price=" + price +
                ", speedBoost=" + speedBoost +
                '}';
    }
}
