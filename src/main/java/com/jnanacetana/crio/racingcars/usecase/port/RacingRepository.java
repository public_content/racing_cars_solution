package com.jnanacetana.crio.racingcars.usecase.port;

import com.jnanacetana.crio.racingcars.domain.entity.Team;

import java.util.List;

public interface RacingRepository {
    public List<Team> findAllTeams();
    public void addTeam(String teamId, String teamName, String carId, Integer funds);
    public void addCar(String carId, String carName, Integer baseSpeed, Integer topSpeed, String partListId);
    public void addPart(String partListId, String partId, Integer price, Integer speedBoost);
}
