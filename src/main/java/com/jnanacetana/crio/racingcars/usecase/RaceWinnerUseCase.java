package com.jnanacetana.crio.racingcars.usecase;

import com.jnanacetana.crio.racingcars.domain.entity.Car;
import com.jnanacetana.crio.racingcars.domain.entity.Part;
import com.jnanacetana.crio.racingcars.domain.entity.Team;
import com.jnanacetana.crio.racingcars.usecase.port.RaceWinnerResponse;
import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class RaceWinnerUseCase {
    private RacingRepository racingRepository;

    public RaceWinnerUseCase(RacingRepository racingRepository) {
        this.racingRepository = racingRepository;
    }

    public RaceWinnerResponse getRaceWinner() throws NoSuchElementException {
            List<Team> teams = racingRepository.findAllTeams();
            List<RaceWinnerResponse> potentialRaceWinnerResponseList = teams
                    .stream()
                    .map(this::getFastestCarInTeam)
                    .collect(Collectors.toList());
            RaceWinnerResponse raceWinnerResponse = potentialRaceWinnerResponseList
                    .stream()
                    .max(Comparator.comparing(RaceWinnerResponse::getSpeed))
                    .orElseThrow(NoSuchElementException::new);
            return raceWinnerResponse;
    }

    public RaceWinnerResponse getFastestCarInTeam(Team team) throws NoSuchElementException {
       List<RaceWinnerResponse> potentialBestCarList = team.getCars()
               .stream()
               .map(car -> calculateMaximumSpeedCarSpecs(car, team.getFunds()))
               .collect(Collectors.toList());
       RaceWinnerResponse raceWinnerResponse = potentialBestCarList
               .stream()
               .max(Comparator.comparing(RaceWinnerResponse::getSpeed))
               .orElseThrow(NoSuchElementException::new);
       raceWinnerResponse.setTeamName(team.getName());
        return raceWinnerResponse;
    }

    public RaceWinnerResponse calculateMaximumSpeedCarSpecs(Car car, Integer funds) {
        List<Part> parts = car.getParts();
        List <String> attachedParts = new ArrayList<>();
        int numParts = parts.size();
        int maxSpeedBoost = car.getTopSpeed() - car.getBaseSpeed();
        int[][] speedKnapSack = new int[numParts + 1][funds + 1];
        for (int i = 0; i < numParts + 1; i++) {
            for (int w = 0; w < funds + 1; w++) {
                if (i == 0 || w == 0) {
                    speedKnapSack[i][w] = 0;
                } else if (parts.get(i - 1).getPrice() <= w) {
                    if (parts.get(i - 1).getSpeedBoost() + speedKnapSack[i - 1][w - parts.get(i - 1).getPrice()] < maxSpeedBoost) {
                        speedKnapSack[i][w] = Math.max(parts.get(i - 1).getSpeedBoost() + speedKnapSack[i - 1][w - parts.get(i - 1).getPrice()], speedKnapSack[i - 1][w]);
                    } else {
                        speedKnapSack[i][w] = speedKnapSack[i-1][w];
                    }
                }
                else{
                    speedKnapSack[i][w] = speedKnapSack[i-1][w];
                }
            }
        }
        int result = speedKnapSack[numParts][funds];
        Integer w = funds;
        for(int i = numParts; i > 0 ; i--){
            if(result <= 0) break;
            if ( result == speedKnapSack[i-1][w]){
            }else{
                attachedParts.add(parts.get(i-1).getPartId());
                result = result - parts.get(i-1).getSpeedBoost();
                w = w - parts.get(i-1).getPrice();
            }
        }

        RaceWinnerResponse raceWinnerResponse = new RaceWinnerResponse();
        raceWinnerResponse.setCarName(car.getName());
        raceWinnerResponse.setFunds(funds);
        raceWinnerResponse.setSpeed(car.getBaseSpeed() + speedKnapSack[numParts][funds]);
        raceWinnerResponse.setParts(attachedParts);

        return raceWinnerResponse;

    }
}
