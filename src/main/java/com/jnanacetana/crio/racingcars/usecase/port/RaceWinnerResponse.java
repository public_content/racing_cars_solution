package com.jnanacetana.crio.racingcars.usecase.port;

import java.util.List;

public class RaceWinnerResponse {
    private String teamName;
    private String carName;
    private Integer speed;
    private Integer funds;
    private List<String> parts;

    public RaceWinnerResponse(){}
    public RaceWinnerResponse(String teamName, String carName, Integer speed, Integer funds, List<String> parts) {
        this.teamName = teamName;
        this.carName = carName;
        this.speed = speed;
        this.funds = funds;
        this.parts = parts;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public List<String> getParts() {
        return parts;
    }

    public void setParts(List<String> parts) {
        this.parts = parts;
    }

    @Override
    public String toString() {
        return "RaceWinnerResponse{" +
                "teamName='" + teamName + '\'' +
                ", carName='" + carName + '\'' +
                ", speed=" + speed +
                ", funds=" + funds +
                ", parts=" + parts +
                '}';
    }
}
