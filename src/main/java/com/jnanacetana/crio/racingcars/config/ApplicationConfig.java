package com.jnanacetana.crio.racingcars.config;

import com.jnanacetana.crio.racingcars.adapter.command.GetWinnerCommand;
import com.jnanacetana.crio.racingcars.adapter.csvprocessor.CarCSVProcessor;
import com.jnanacetana.crio.racingcars.adapter.csvprocessor.PartCSVProcessor;
import com.jnanacetana.crio.racingcars.adapter.csvprocessor.TeamCSVProcessor;
import com.jnanacetana.crio.racingcars.adapter.repository.RacingRepositoryImpl;
import com.jnanacetana.crio.racingcars.usecase.RaceWinnerUseCase;
import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;

public class ApplicationConfig {
    private final RacingRepository racingRepository = new RacingRepositoryImpl();
    private final RaceWinnerUseCase raceWinnerUseCase = new RaceWinnerUseCase(racingRepository);

    public TeamCSVProcessor getTeamCSVProcessor(){
        return new TeamCSVProcessor(racingRepository);
    }

    public CarCSVProcessor getCarCSVProcessor(){
        return new CarCSVProcessor(racingRepository);
    }

    public PartCSVProcessor getPartCSVProcessor(){
        return new  PartCSVProcessor(racingRepository);
    }

    public GetWinnerCommand getWinnerCommand() { return new GetWinnerCommand(raceWinnerUseCase);}
}
