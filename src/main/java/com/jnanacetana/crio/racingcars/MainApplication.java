package com.jnanacetana.crio.racingcars;

import com.jnanacetana.crio.racingcars.adapter.command.CommandInvoker;
import com.jnanacetana.crio.racingcars.config.ApplicationConfig;
import com.jnanacetana.crio.racingcars.usecase.port.RaceWinnerResponse;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MainApplication {


    public static void main(String[] args) throws IOException {
        var config = new ApplicationConfig();
        var commandInvoker = new CommandInvoker();
        var teamProcessor = config.getTeamCSVProcessor();
        var carProcessor = config.getCarCSVProcessor();
        var partProcessor = config.getPartCSVProcessor();
        var winnerCommand = config.getWinnerCommand();

        teamProcessor.execute(new FileReader(args[0]));
        carProcessor.execute(new FileReader(args[1]));
        partProcessor.execute(new FileReader(args[2]));

        commandInvoker.register("GET_RACE_WINNER",winnerCommand);
        commandInvoker.execute("GET_RACE_WINNER",new ArrayList<>());

    };
}
