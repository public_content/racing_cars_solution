package com.jnanacetana.crio.racingcars.domain.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarTest {
    // Tests for Car Entity Beahviours
    @Test
    public void setBaseSpeedOfCar(){
        Car car = new Car();
        car.setBaseSpeed(100);
        Assertions.assertEquals(100,car.getBaseSpeed());
    }
}
