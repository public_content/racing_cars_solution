package com.jnanacetana.crio.racingcars.usecase;

import com.jnanacetana.crio.racingcars.adapter.repository.RacingRepositoryImpl;
import com.jnanacetana.crio.racingcars.domain.entity.Car;
import com.jnanacetana.crio.racingcars.domain.entity.Part;
import com.jnanacetana.crio.racingcars.usecase.port.RaceWinnerResponse;
import com.jnanacetana.crio.racingcars.usecase.port.RacingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class RaceWinnerUseCaseTest {
    @Test
    public void calculateMaximumSpeedCarSpecsTest(){
        RaceWinnerResponse raceWinnerResponse = new RaceWinnerResponse();
        raceWinnerResponse.setCarName("MODEL_DF178");
        raceWinnerResponse.setFunds(32770);
        raceWinnerResponse.setSpeed(248);
        raceWinnerResponse.setParts(new ArrayList<>(){
            {
                add("PART_36CB7");
                add("PART_ACBF3");
                add("PART_3C7D6");
                add("PART_AEE00");
                add("PART_A2E4D");
                add("PART_A5005");
            }
        });
        RacingRepository racingRepository = new RacingRepositoryImpl();
        RaceWinnerUseCase raceWinnerUseCase = new RaceWinnerUseCase(racingRepository);
        Car car = new Car("CARID_2047","MODEL_DF178",140,252, new ArrayList<Part>(){
            {
                add(new Part("PART_A5005",882,14));add(new Part("PART_A2E4D",3256,32));
                add(new Part("PART_AEE00",4788,30));add(new Part("PART_3C7D6",524,11));
                add(new Part("PART_FCCFB",2454,36));add(new Part("PART_ACBF3",1502,14));
                add(new Part("PART_F5DDB",4743,57));add(new Part("PART_26254",2750,28));
                add(new Part("PART_F7574",561,15));add(new Part("PART_03E12",3262,34));
                add(new Part("PART_1F434",4527,66));add(new Part("PART_36CB7",984,7));
                add(new Part("PART_30ECC",2387,36));add(new Part("PART_8C893",4509,40));
                add(new Part("PART_D5EB6",2462,25));add(new Part("PART_A45EB",3607,20));
                add(new Part("PART_DD197",2860,26));add(new Part("PART_405C0",4392,63));
                add(new Part("PART_532D5",906,8));add(new Part("PART_A208E",3882,20));
                add(new Part("PART_513DB",3693,32));add(new Part("PART_7839A",502,17));
                add(new Part("PART_AB98B",2858,20));add(new Part("PART_E4F13",3775,28));
                add(new Part("PART_53C0E",3918,24));add(new Part("PART_2320A",1890,12));
                add(new Part("PART_8FE6D",252,19));add(new Part("PART_0F995",2654,33));
                add(new Part("PART_D4A83",1316,7));add(new Part("PART_5E8FD",991,18));
                add(new Part("PART_DD284",3435,35));add(new Part("PART_8AF0",1630,14));
                add(new Part("PART_5EA86",3315,26));
            }
        });
        Assertions.assertEquals(raceWinnerResponse.toString(),raceWinnerUseCase.calculateMaximumSpeedCarSpecs(car,32770).toString());
    }

    @Test
    public void getFastestCarInTeamTest(){
        // Test getFastestCarInTeam of RaceWinnerUseCase Object
    }

    @Test
    public void  getRaceWinnerTest() {
        // Test  getRaceWinner() of  RaceWinnerResponse Object
    }
}