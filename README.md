

Inside a parent directory,

Build the code:-

```
./gradlew clean build

```
A jar file will be generated at build/libs folder.

Execute the code:-

```
java -jar ./build/libs/<jar file> <Teams.csv file path> <Cars.csv file path> <Parts.csv file path>

E.g:-
java -jar ./build/libs/geektrust-1.0-SNAPSHOT.jar /home/yakshit/IdeaProjects/RacingCars/src/main/resources/Teams.csv  /home/yakshit/IdeaProjects/RacingCars/src/main/resources/Cars.csv  /home/yakshit/IdeaProjects/RacingCars/src/main/resources/Parts.csv 

```

Expected Output:- 

```
RaceWinnerResponse{teamName='Mercedes', carName='MODEL_DF178', speed=248, funds=32770, parts=[PART_36CB7, PART_ACBF3, PART_3C7D6, PART_AEE00, PART_A2E4D, PART_A5005]}
```